import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.log4j.Level;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.hp.hpl.jena.assembler.Assembler;
import com.hp.hpl.jena.ontology.*;
import com.hp.hpl.jena.query.Dataset;
import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ReadWrite;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.query.ResultSetFormatter;
import com.hp.hpl.jena.rdf.model.InfModel;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.reasoner.Reasoner;
import com.hp.hpl.jena.reasoner.ReasonerRegistry;
import com.hp.hpl.jena.shared.PrefixMapping;
import com.hp.hpl.jena.tdb.TDBFactory;
import com.hp.hpl.jena.util.FileManager;
public class ModelAPI {
	
	public static void main(String[] args) throws IOException
	{
		// Onemogo�imo izpisovanje opozoril
        @SuppressWarnings("unchecked")
        List<Logger> loggers = Collections.<Logger>list(LogManager.getCurrentLoggers());
        loggers.add(LogManager.getRootLogger());
        for (Logger logger : loggers)
            logger.setLevel(Level.OFF);
        
        // Nastavitve za lep�i izpis
        PrefixMapping pm = PrefixMapping.Factory.create();
        pm.setNsPrefix("", "http://www.test.org/VidmarJernej/diploma#");
        
        // Dodajanje modela
        System.out.print("Nalagam model ... ");
        OntModel m = ModelFactory.createOntologyModel(OntModelSpec.RDFS_MEM_RDFS_INF);
        
        m.read(
                new FileReader(
                "C:\\Users\\FRI\\workspace\\DIplomskaNaloga_ModelAPI\\test_model.n3"), 
                "http://www.test.org/VidmarJernej/diploma#", 
                "N3"
        		);
        System.out.println("OK" + "\n");
        
        // Dodajanje datotek s podatki      
        System.out.print("Nalagam datoteko s podatki ... ");
        Model data = ModelFactory.createDefaultModel();
        		data.read(new FileReader("C:\\Users\\FRI\\workspace\\DIplomskaNaloga_ModelAPI\\006893251.rdf"),
        				"",
        				"RDF/XML");
		System.out.println("OK" + "\n");
               
	       
		// Branje assemblerja za D2RQ platformo
		System.out.print("Nalagam D2RQ assembler... ");
        Model assemblerSpecs = FileManager.get().loadModel("src/assembler.ttl");
        Resource modelSpec = assemblerSpecs.createResource(assemblerSpecs.expandPrefix("eg:myModel"));
        Model assembler = Assembler.general.openModel(modelSpec);
        m.add(assembler); // dodajanje k obstojecemu modelu
        assembler.close();
        System.out.println("OK"+"\n");
        
       
		// povezovanje modela in podatkov
        Reasoner reasoner = ReasonerRegistry.getRDFSReasoner();
        reasoner = reasoner.bindSchema(m);
        InfModel inf_model = ModelFactory.createInfModel(reasoner, data);
        
        // Poizvedba 1
        String query = 
                "PREFIX    : <http://www.test.org/VidmarJernej/diploma#> " +
                "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> " +
                "SELECT * WHERE {?avtor rdf:type :Avtor}";
        System.out.println("Izvajam poizvedbo '" + query.replaceAll("\\s+", " ") + "'.");
        ResultSet rs = QueryExecutionFactory.create(QueryFactory.create(query), inf_model).execSelect();
        ResultSetFormatter.out(System.out, rs, pm);           

        // Poizvedba 2
        String query1 = 
                "PREFIX    : <http://www.test.org/VidmarJernej/diploma#> " +
                "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> " +
                "SELECT * WHERE {?knjiga rdf:type :Knjiga}";
        System.out.println("Izvajam poizvedbo '" + query1.replaceAll("\\s+", " ") + "'.");
        rs = QueryExecutionFactory.create(QueryFactory.create(query1), inf_model).execSelect();
        ResultSetFormatter.out(System.out, rs, pm);
        
        // Poizvedba 3 
        String query2 = 
                "PREFIX    : <http://www.test.org/VidmarJernej/diploma#> " +
                "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> " +
                "SELECT ?book ?title ?ISBN WHERE { ?book rdf:type :Knjiga. OPTIONAL{?book :naslov ?title; :idKnjige ?ISBN} } ORDER BY ?instance";
        System.out.println("Izvajam poizvedbo '" + query2.replaceAll("\\s+", " ") + "'."); 	
        ResultSet rez = QueryExecutionFactory.create(QueryFactory.create(query2), inf_model).execSelect();
        ResultSetFormatter.out(System.out, rez);
        
	}
	
	private static void CreateTDB() throws IOException
	{
		System.out.print("Ustvarjam bazo ... ");
		String source = "C:\\Users\\FRI\\workspace\\DIplomskaNaloga_ModelAPI\\006893251.rdf";
		String directory = "C:\\Users\\FRI\\Documents\\Jena\\tdb";
		Dataset dataset = TDBFactory.createDataset(directory);
		Model tdb = dataset.getDefaultModel();
		dataset.begin(ReadWrite.READ);		
		tdb.read(new FileReader(source), "", "RDF/XML");
		dataset.commit();
		dataset.end();
		tdb.close();
		dataset.close();
		System.out.println("OK" + "\n");		
	}
	
	private static Model ReturnTDBmodel() 
	{
		System.out.print("Pridobivam TDB bazo ... ");
		String directory = "C:\\Users\\FRI\\Documents\\Jena\\tdb";
		Dataset ds = TDBFactory.createDataset(directory);
		System.out.println("OK" + "\n");
		return ds.getDefaultModel();		
	}
}
